package sample.mvc.dto;

public class BookingDto {

    private String code;
    private UserDto user;
    private boolean checkIn;
    private String seat;

    public String getCode() {
        return code;
    }

    public boolean isCheckIn() {
        return checkIn;
    }

    public void setCheckIn(boolean checkIn) {
        this.checkIn = checkIn;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}
    
    
}
