package sample.mvc.dto;

public class UserDto {

    private String name;
    private String surname;
    private String email;
    private String mobilePhone;
    private PassportDto passportDto;
    
    public UserDto() {}

    public UserDto(String name, String surname, String email, String mobilePhone, PassportDto passportDto) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.mobilePhone = mobilePhone;
		this.passportDto = passportDto;
	}
    
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public PassportDto getPassportDto() {
        return passportDto;
    }

    public void setPassportDto(PassportDto passportDto) {
        this.passportDto = passportDto;
    }
}
