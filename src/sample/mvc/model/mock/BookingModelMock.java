package sample.mvc.model.mock;

import sample.mvc.dto.BookingDto;
import sample.mvc.dto.PassportDto;
import sample.mvc.dto.UserDto;
import sample.mvc.model.BookingModel;
import sample.mvc.util.Sex;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingModelMock implements BookingModel {

    private final List<BookingDto> bookings;

    public BookingModelMock() {
        bookings = new ArrayList<>();
        bookings.add(create("DRNDNL", false, null, "Daniele", "Adriano", "d.a@gmail.com", "3322587410", Sex.MALE));
        bookings.add(create("IMKRNT", true, "22A", "Renato", "Imaku", "r.i@gmail.com", "3322587411", Sex.MALE));
        bookings.add(create("MMJNRS", false, null, "Memaj", "Noris", "m.n@gmail.com", "3322587412", Sex.MALE));
        bookings.add(create("LPPFLP", false, null, "Lippolis", "Filippa", "l.f@gmail.com", "3322587413", Sex.FEMALE));
    }

    @Override
    public BookingDto search(String bookingCode) {
    	BookingDto booking = null;
    	for(int i=0; i<bookings.size(); i++) {
    		if(bookingCode.equalsIgnoreCase(bookings.get(i).getCode())) {
    			booking = bookings.get(i);
    			break;
    		}
    	}
    	return booking;
    }

    @Override
    public List<BookingDto> findAll() {
        return bookings;
    }

    @Override
    public void update(BookingDto bookingDto) {
        // do nothing
    }

	@Override
	public List<BookingDto> findAllCompleteCheckIn() {
		List<BookingDto> bookingsCCI = new ArrayList<BookingDto>();
		for(int i=0; i<bookings.size(); i++) {
			if(bookings.get(i).isCheckIn()) {
				bookingsCCI.add(bookings.get(i));
			}
		}
		return bookingsCCI;
	}

	@Override
	public List<BookingDto> findAllNotCompleteCheckIn() {
		List<BookingDto> bookingsNCCI = new ArrayList<BookingDto>();
		for(int i=0; i<bookings.size(); i++) {
			if(!bookings.get(i).isCheckIn()) {
				bookingsNCCI.add(bookings.get(i));
			}
		}
		return bookingsNCCI;
	}

	@Override
	public boolean delete(String bookingCode) {
		Integer idx = null;
		for(int i=0; i<bookings.size(); i++) {
			if(bookingCode.equalsIgnoreCase(bookings.get(i).getCode())) {
				idx = i;
				break;
			}
		}
		boolean success = false;
		if(idx != null) {
			bookings.remove(idx.intValue());
			success = true;
		}
		return success;
	}

	@Override
	public boolean insert(BookingDto bookingDto) {
		BookingDto booking = search(bookingDto.getCode());
		boolean success = false;
		if(booking == null) {
			success = true;
			bookings.add(bookingDto);
		} 
		return success;
	}
	
	private BookingDto create(String bookingCode, boolean checkIn, String seat, String name, String surname, String email, String mobilePhone, Sex sex) {
        BookingDto booking = new BookingDto();
        booking.setCode(bookingCode);
        booking.setSeat(seat);
        booking.setCheckIn(checkIn);
        UserDto user = new UserDto();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setMobilePhone(mobilePhone);
        booking.setUser(user);
        
        if(checkIn) {
        	PassportDto passport = new PassportDto();
        	passport.setBirthDate(new Date());
        	passport.setEndDate(new Date());
        	passport.setCitizenship("Italiana");
        	passport.setCode("P-" + booking.getCode());
        	passport.setSex(sex);
        	user.setPassportDto(passport);
        }
        
        return booking;
    }
}
