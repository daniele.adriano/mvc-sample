package sample.mvc.model;


import sample.mvc.dto.BookingDto;

import java.util.List;

public interface BookingModel {

    BookingDto search(String bookingCode);

    List<BookingDto> findAll();
    
    List<BookingDto> findAllCompleteCheckIn();
    
    List<BookingDto> findAllNotCompleteCheckIn();

    void update(BookingDto bookingDto);
    
    boolean delete(String bookingCode);
    
    boolean insert(BookingDto bookingDto);

}