package sample.mvc.cli;

/**
 * Common CLI view interface
 */
public interface CliView {

    /**
     * Show specific view
     */
    void update();

}
