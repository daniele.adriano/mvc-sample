package sample.mvc.cli;

/**
 * Common interface for views with "dynamic data"
 *
 * @param <T>
 */
public interface CliDtoView<T> extends CliView {

    /**
     * Set view DTO
     *
     * @param dto
     */
    void setDto(T dto);

}
