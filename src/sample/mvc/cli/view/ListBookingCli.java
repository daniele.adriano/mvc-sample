package sample.mvc.cli.view;

import sample.mvc.cli.CliDtoView;
import sample.mvc.dto.BookingDto;

import java.util.List;

/**
 * Shows bookings and check-in status
 */
public class ListBookingCli implements CliDtoView<List<BookingDto>> {

    private List<BookingDto> bookings;

    @Override
    public void update() {
        System.out.println("\n.:: Booking codes ::.");
        for (BookingDto booking : bookings) {
            System.out.println(booking.getCode() + '\t' + (booking.isCheckIn() ? "Check-in DONE" : "Check-in PENDING"));
        }
    }

    public void setDto(List<BookingDto> bookings) {
        this.bookings = bookings;
    }
}
