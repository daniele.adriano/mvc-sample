package sample.mvc.cli.view;

import sample.mvc.cli.CliView;
import sample.mvc.cli.controller.BookingController;

import java.util.Scanner;

/**
 * Asks the user to insert the booking code of booking to be searched. Specific controller is invoked to search
 * provided code
 */
public class SearchBookingCli implements CliView {

    private final Scanner in;
    private BookingController bookingController;

    public SearchBookingCli() {
        this.in = new Scanner(System.in);
    }

    @Override
    public void update() {
        System.out.println("\n.:: Booking Detail ::.");
        System.out.print("Enter booking code: ");
        String bookingCode = in.next();
        bookingController.search(bookingCode);
    }

    public void setController(BookingController bookingController) {
        this.bookingController = bookingController;
    }
}
