package sample.mvc.cli.view;


import sample.mvc.cli.CliDtoView;
import sample.mvc.cli.controller.CheckInController;
import sample.mvc.dto.BookingDto;

import java.util.Scanner;

/**
 * Shows booking details and asks for check-in data
 */
public class CheckInCli implements CliDtoView<BookingDto> {

    private final Scanner in;
    private CheckInController checkInController;
    private BookingDto bookingDto;

    public CheckInCli() {
        this.in = new Scanner(System.in);
    }

    @Override
    public void update() {
        System.out.println("\n.:: Check-in ::.");
        if (bookingDto == null) {
            System.out.println("Unable to find booking with pending check-ing having provided code...");
        } else {
            System.out.println("Booking code: " + bookingDto.getCode());
            System.out.println("User name: " + bookingDto.getUser().getName());
            System.out.println("User surname: " + bookingDto.getUser().getSurname());
            System.out.print("\nPlease enter passport code: ");
            String passportCode = in.next();
            checkInController.checkIn(passportCode);
        }
    }

    public void setDto(BookingDto bookingDto) {
        this.bookingDto = bookingDto;
    }

    public void setController(CheckInController checkInController) {
        this.checkInController = checkInController;
    }
}
