package sample.mvc.cli.view;

import sample.mvc.cli.CliView;
import sample.mvc.cli.controller.ListBookingController;
import sample.mvc.cli.controller.MenuController;
import sample.mvc.cli.controller.SearchBookingController;
import sample.mvc.cli.controller.SearchCheckInController;

import java.util.Scanner;

/**
 * Shows menu options, users choices are delivered to specific controller
 */
public class MenuCli implements CliView {

    private final Scanner in;
    private ListBookingController listBookingController;
    private SearchBookingController searchBookingController;
    private SearchCheckInController searchCheckInController;
    private MenuController menuController;

    public MenuCli() {
        this.in = new Scanner(System.in);
    }

    @Override
    public void update() {
        System.out.println("\n.:: Check in magic software ::.");
        System.out.println("1. list all bookings");
        System.out.println("2. show booking details");
        System.out.println("3. check-in");
        System.out.print("Choose an option (or type anything else to exit): ");
        String option = in.next();
        switch (option) {
            case "1":
                listBookingController.updateView();
                break;
            case "2":
                searchBookingController.updateView();
                break;
            case "3":
                searchCheckInController.updateView();
                break;
            default:
                menuController.exit(true);
                break;
        }
    }

    public void setController(MenuController menuController, ListBookingController listBookingController, SearchBookingController searchBookingController, SearchCheckInController searchCheckInController) {
        this.menuController = menuController;
        this.listBookingController = listBookingController;
        this.searchBookingController = searchBookingController;
        this.searchCheckInController = searchCheckInController;
    }
}
