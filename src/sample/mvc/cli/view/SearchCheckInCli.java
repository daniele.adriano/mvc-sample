package sample.mvc.cli.view;

import sample.mvc.cli.CliView;
import sample.mvc.cli.controller.CheckInController;

import java.util.Scanner;

/**
 * Asks the user to insert the booking code of booking to be searched. Specific controller is invoked to search
 * provided code
 */
public class SearchCheckInCli implements CliView {

    private final Scanner in;
    private CheckInController checkInController;

    public SearchCheckInCli() {
        this.in = new Scanner(System.in);
    }

    @Override
    public void update() {
        System.out.println("\n.:: Check-in ::.");
        System.out.print("Enter booking code: ");
        String bookingCode = in.next();
        checkInController.search(bookingCode);
    }

    public void setController(CheckInController checkInController) {
        this.checkInController = checkInController;
    }
}
