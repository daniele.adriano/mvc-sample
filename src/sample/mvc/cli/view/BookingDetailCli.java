package sample.mvc.cli.view;


import sample.mvc.cli.CliDtoView;
import sample.mvc.dto.BookingDto;

/**
 * Shows booking details
 */
public class BookingDetailCli implements CliDtoView<BookingDto> {

    private BookingDto bookingDto;

    @Override
    public void update() {
        System.out.println("\n.:: Booking details ::.");
        if (bookingDto == null) {
            System.out.println("Unable to find booking with provided code...");
        } else {
            System.out.println("Booking code: " + bookingDto.getCode());
            System.out.println("Check-in: " + (bookingDto.isCheckIn() ? "DONE" : "PENDING"));
            System.out.println("User name: " + bookingDto.getUser().getName());
            System.out.println("User surname: " + bookingDto.getUser().getSurname());
            System.out.println("Passport code: " + (bookingDto.getUser().getPassportDto() != null ? bookingDto.getUser().getPassportDto().getCode() : "-"));
        }
    }

    @Override
    public void setDto(BookingDto bookingDto) {
        this.bookingDto = bookingDto;
    }
}
