package sample.mvc.cli.controller;

import sample.mvc.cli.CliDtoView;
import sample.mvc.dto.BookingDto;
import sample.mvc.model.BookingModel;

public class BookingController {

    private final CliDtoView<BookingDto> bookingCli;
    private final BookingModel bookingModel;

    private BookingDto bookingDto;

    public BookingController(CliDtoView<BookingDto> bookingCli, BookingModel bookingModel) {
        this.bookingCli = bookingCli;
        this.bookingModel = bookingModel;
    }

    public void updateView() {
        bookingCli.setDto(bookingDto);
        bookingCli.update();
    }

    public void search(String bookingCode) {
        bookingDto = bookingModel.search(bookingCode);
        updateView();
    }

}
