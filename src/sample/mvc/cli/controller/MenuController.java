package sample.mvc.cli.controller;


import sample.mvc.cli.CliView;

public class MenuController {

    private final CliView menuCli;

    private Boolean exit = false;

    public MenuController(CliView menuCli) {
        this.menuCli = menuCli;
    }

    public void updateView() {
        while (!exit) {
            menuCli.update();
        }
    }

    public void exit(Boolean exit) {
        this.exit = exit;
    }

}
