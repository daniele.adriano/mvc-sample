package sample.mvc.cli.controller;

import sample.mvc.cli.CliView;

public class SearchBookingController  {

    private final CliView searchBookingCli;

    public SearchBookingController(CliView searchBookingCli) {
        this.searchBookingCli = searchBookingCli;
    }

    public void updateView() {
        searchBookingCli.update();
    }

}
