package sample.mvc.cli.controller;

import sample.mvc.cli.CliDtoView;
import sample.mvc.dto.BookingDto;
import sample.mvc.dto.PassportDto;
import sample.mvc.model.BookingModel;

public class CheckInController {

    private final CliDtoView<BookingDto> checkInCli;
    private final BookingModel bookingModel;

    private BookingDto bookingDto;

    public CheckInController(CliDtoView<BookingDto> checkInCli, BookingModel bookingModel) {
        this.checkInCli = checkInCli;
        this.bookingModel = bookingModel;
    }

    public void updateView() {
        checkInCli.setDto(bookingDto);
        checkInCli.update();
    }

    public void search(String bookingCode) {
        // TODO search booking with pending check-in
        bookingDto = bookingModel.search(bookingCode);
        updateView();
    }

    public void checkIn(String passportCode) {
        bookingDto.setCheckIn(true);
        bookingDto.getUser().setPassportDto(new PassportDto());
        bookingDto.getUser().getPassportDto().setCode(passportCode);
        bookingModel.update(bookingDto);
    }
}
