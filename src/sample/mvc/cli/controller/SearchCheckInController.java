package sample.mvc.cli.controller;

import sample.mvc.cli.CliView;

public class SearchCheckInController {

    private final CliView searchCheckInCli;

    public SearchCheckInController(CliView searchBookingCli) {
        this.searchCheckInCli = searchBookingCli;
    }

    public void updateView() {
        searchCheckInCli.update();
    }

}
