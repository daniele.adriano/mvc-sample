package sample.mvc.cli.controller;

import sample.mvc.cli.CliDtoView;
import sample.mvc.dto.BookingDto;
import sample.mvc.model.BookingModel;

import java.util.List;

public class ListBookingController {

    private final CliDtoView<List<BookingDto>> listBookingCli;
    private final BookingModel bookingModel;

    public ListBookingController(CliDtoView<List<BookingDto>> listBookingCli, BookingModel bookingModel) {
        this.listBookingCli = listBookingCli;
        this.bookingModel = bookingModel;
    }

    public void updateView() {
        List<BookingDto> bookings = bookingModel.findAll();
        listBookingCli.setDto(bookings);
        listBookingCli.update();
    }

}
