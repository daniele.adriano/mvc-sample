package sample.mvc;

import sample.mvc.cli.controller.*;
import sample.mvc.cli.view.*;
import sample.mvc.model.BookingModel;
import sample.mvc.model.mock.BookingModelMock;

public class CliApplication {

    public static void main(String[] args) {
        // Setup View
        MenuCli menuCli = new MenuCli();
        ListBookingCli listBookingCli = new ListBookingCli();
        SearchBookingCli searchBookingCli = new SearchBookingCli();
        BookingDetailCli bookingCli = new BookingDetailCli();
        SearchCheckInCli searchCheckInCli = new SearchCheckInCli();
        CheckInCli checkInCli = new CheckInCli();

        // Setup Model
        BookingModel bookingModel = new BookingModelMock();

        // Setup controller
        MenuController menuController = new MenuController(menuCli);
        ListBookingController listBookingController = new ListBookingController(listBookingCli, bookingModel);
        SearchBookingController searchBookingController = new SearchBookingController(searchBookingCli);
        BookingController bookingController = new BookingController(bookingCli, bookingModel);
        SearchCheckInController searchCheckInController = new SearchCheckInController(searchCheckInCli);
        CheckInController checkInController = new CheckInController(checkInCli, bookingModel);

        // Associate controller to view
        menuCli.setController(menuController, listBookingController, searchBookingController, searchCheckInController);
        searchBookingCli.setController(bookingController);
        searchCheckInCli.setController(checkInController);
        checkInCli.setController(checkInController);

        // Entry point
        menuController.updateView();
    }

}
